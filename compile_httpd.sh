#!/bin/bash

wget https://mirrors.up.pt/pub/apache/httpd/httpd-2.4.35.tar.gz
tar xf httpd-2.4.35.tar.gz

cd httpd-2.4.35

make clean
./configure
make --jobs
