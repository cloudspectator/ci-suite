#!/bin/bash
cpu_number=$(grep 'processor' /proc/cpuinfo | wc -l)
pipe_number=$(awk -F= '/concurrent/ {print $2}' | /etc/gitlab-runner/config.toml)
default_job_number=$((pipe_number / cpu_number))

wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.18.12.tar.xz
tar xf linux-4.18.12.tar.xz

cd linux-4.18.12

make clean
make olddefconfig
make --jobs 2